<?php
/**
 * ObjectContainer - Container to store Objects
 *
 * @author Dominik Tomasi <dominik.tomasi@gmail.com>
 * @copyright tomasiMEDIA 2014
 *
 */

namespace Dtomasi\Containers\Helpers;

/**
 * Class ObjectInfo
 * @package Dtomasi\Containers
 */
class ObjectInfo
{
    /**
     * The Real-Object
     * @var object
     */
    protected $object;

    /**
     * SplObjectHash
     * @var string
     */
    protected $splHash;

    /**
     * provided Key
     * @var null|string
     */
    protected $key;

    /**
     * Class-String
     * @var string
     */
    protected $class;

    /**
     * Parent-Class-String
     * @var string
     */
    protected $parent;

    /**
     * Array of Tags for search
     * @var array
     */
    protected $tags;

    /**
     * Init
     * @param $object
     * @param null $strKey
     * @param array $arrTags
     * @throws \InvalidArgumentException
     */
    public function __construct($object, $strKey = null, array $arrTags = array())
    {

        if (!is_object($object)) {
            throw new \InvalidArgumentException('$object must be a valid php-object');
        }

        $this->object = $object;
        $this->key = $strKey;
        $this->splHash = spl_object_hash($this->object);
        $this->class = get_class($this->object);
        $this->parent = get_parent_class($this->object);

        $this->tags = $arrTags;
    }

    /**
     * @return object
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @return null|string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->splHash;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @return \ReflectionClass
     */
    public function getReflectionClass()
    {
        return new \ReflectionClass($this->object);
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param $strTag
     * @return bool
     */
    public function hasTag($strTag)
    {
        return (array_key_exists($strTag, array_flip($this->tags)));
    }
} 