<?php
/**
 * ObjectContainer - Container to store Objects
 *
 * @author Dominik Tomasi <dominik.tomasi@gmail.com>
 * @copyright tomasiMEDIA 2014
 *
 */

namespace Dtomasi\Containers;

/**
 * Interface ServiceInterface
 * @package Dtomasi\Containers
 */
interface ServiceInterface {

    /**
     * Get the name
     *
     * @return string
     */
    public function getName();

    /**
     * Get the tags for searching
     *
     * @return array
     */
    public function getTags();

} 