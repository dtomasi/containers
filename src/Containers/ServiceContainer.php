<?php
/**
 * ObjectContainer - Container to store Objects
 *
 * @author Dominik Tomasi <dominik.tomasi@gmail.com>
 * @copyright tomasiMEDIA 2014
 *
 */
namespace Dtomasi\Containers;

/**
 * Class ServiceContainer
 * @package Dtomasi\Containers
 */
class ServiceContainer extends ObjectContainer
{

    /**
     * Add a Service to the Container
     *
     * @param ServiceInterface $service
     * @return string|void
     */
    public function add(ServiceInterface $service)
    {
        return parent::addObject($service, $service->getName(), $service->getTags());
    }

    /**
     * Get a Service from Container
     *
     * @param $name
     * @return mixed
     */
    public function get($name)
    {
        return parent::getObject($name);
    }
} 