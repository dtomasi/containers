<?php
/**
 * ObjectContainer - Container to store Objects
 *
 * @author Dominik Tomasi <dominik.tomasi@gmail.com>
 * @copyright tomasiMEDIA 2014
 *
 */
namespace Dtomasi\Containers;

use Dtomasi\Collections\ArrayCollection;
use Dtomasi\Containers\Helpers\ObjectInfo;

/**
 * Class ObjectContainer
 * @package Dtomasi\Containers
 */
class ObjectContainer
{

    /**
     * The ObjectContainer
     * @var ArrayCollection
     */
    protected $container;

    /**
     * Set the Container
     */
    public function __construct()
    {
        $this->container = new ArrayCollection();
    }

    /**
     * Add a Object
     * @param $object
     * @param null $customKey
     * @param array $searchTags
     * @return string
     * @throws \InvalidArgumentException
     */
    public function addObject($object, $customKey = null, array $searchTags = array())
    {
        if (!is_object($object)) {
            throw new \InvalidArgumentException('$object must be a valid php-object');
        }

        $object = new ObjectInfo($object, $customKey, $searchTags);
        $key = ($customKey ? $customKey : $object->getHash());

        $this->container->set($key, $object);
        return $key;
    }

    /**
     * Get a Object
     * @param $key
     * @return mixed
     */
    public function getObject($key)
    {
        /**
         * @var $info ObjectInfo
         */
        $info = $this->container->get($key);
        return $info->getObject();
    }

    /**
     * Get the ObjectInfo
     * @param $key
     * @return mixed|null
     */
    public function getInfo($key)
    {
        return $this->container->get($key);
    }

    /**
     * Check if a Object already exists in Container
     *
     * @param $object
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function hasObject($object)
    {
        if (!is_object($object)) {
            throw new \InvalidArgumentException('$object must be a valid php-object');
        }
        $hash = spl_object_hash($object);
        return ($this->getByHash($hash) == false ? false : true);
    }

    /**
     * Get a ReflectionClass-Object
     * @param $key
     * @return bool|\ReflectionClass
     */
    public function getReflectionClass($key)
    {
        if ($this->container->has($key)) {
            return $this->container->get($key)->getReflectionClass();
        }
        return false;
    }

    /**
     * Find Objects by Parent Class
     * @param $class
     * @return array|bool
     */
    public function findByParent($class)
    {
        return $this->findBy('parent', $class);
    }

    /**
     * Find one object by Parent Class
     * @param $class
     * @return bool
     */
    public function findOneByParent($class)
    {
        return $this->findOneBy('parent', $class);
    }

    /**
     * Find one by Class
     * @param $class
     * @return bool
     */
    public function findOneByClass($class)
    {
        return $this->findOneBy('class', $class);
    }

    /**
     * Find objects by Class
     * @param $class
     * @return array|bool
     */
    public function findByClass($class)
    {
        return $this->findBy('class', $class);
    }

    /**
     * Find objects by SplObjectHash
     * @param $hash
     * @return bool
     */
    public function getByHash($hash)
    {
        return $this->findOneBy('hash', $hash);
    }

    /**
     * Find objects by given Tags
     * @param $tag
     * @return array|bool
     */
    public function findByTag($tag)
    {

        $arrResults = array();

        $this->container->rewind();
        while ($this->container->valid()) {

            if ($this->container->current()->hasTag($tag)) {
                $arrResults[] = $this->container->current()->getObject();
            }

            $this->container->next();
        }
        return (count($arrResults) > 0 ? $arrResults : false);
    }

    /**
     * Find one by Property
     * @param $strProperty
     * @param $value
     * @return bool
     */
    protected function findOneBy($strProperty, $value)
    {

        $strPropertyMethod = 'get' . ucfirst($strProperty);

        $this->container->rewind();
        while ($this->container->valid()) {

            if ($this->container->current()->$strPropertyMethod() === $value) {
                return $this->container->current()->getObject();
            }

            $this->container->next();
        }
        return false;
    }

    /**
     * Find multiple by Property
     * @param $strProperty
     * @param $value
     * @return array|bool
     */
    protected function findBy($strProperty, $value)
    {

        $arrResults = array();
        $strPropertyMethod = 'get' . ucfirst($strProperty);

        $this->container->rewind();
        while ($this->container->valid()) {

            if ($this->container->current()->$strPropertyMethod() === $value) {
                $arrResults[] = $this->container->current()->getObject();
            }

            $this->container->next();
        }
        return (count($arrResults) > 0 ? $arrResults : false);
    }


}