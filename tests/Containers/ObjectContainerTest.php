<?php
/**
 *
 * User: Dominik Tomasi
 * Date: 15.07.14
 * Time: 06:38
 */

namespace Dtomasi\Tests\Containers;

use Dtomasi\Containers\ObjectContainer;
use Dtomasi\Tests\Containers\TestObjects\ChildChildClass;
use Dtomasi\Tests\Containers\TestObjects\ChildClass;
use Dtomasi\Tests\Containers\TestObjects\ParentClass;

class ObjectContainerTest extends \PHPUnit_Framework_TestCase
{

    public function testSetAndGetByHash()
    {

        $c = new ObjectContainer();

        $hash = $c->addObject(new \stdClass());
        $this->assertInstanceOf('\stdClass', $c->getByHash($hash));
    }

    public function testSetAndGet()
    {

        $c = new ObjectContainer();

        $c->addObject(new \stdClass(), 'foo.bar');
        $this->assertInstanceOf('\stdClass', $c->getObject('foo.bar'));
    }

    public function testSetAndFindByTag()
    {

        $c = new ObjectContainer();

        $c->addObject(new \stdClass(), null, array('foo'));
        $c->addObject(new \stdClass(), null, array('foo', 'bar'));
        $c->addObject(new \stdClass(), null, array('foo', 'bar', 'baz'));

        $this->assertTrue(is_array($c->findByTag('foo')) && count($c->findByTag('foo')) == 3);
        $this->assertTrue(is_array($c->findByTag('bar')) && count($c->findByTag('bar')) == 2);
        $this->assertTrue(is_array($c->findByTag('baz')) && count($c->findByTag('baz')) == 1);
    }

    public function testSetAndFindByClass()
    {

        $c = new ObjectContainer();

        $c->addObject(new ParentClass());
        $c->addObject(new ChildClass());

        $this->assertInstanceOf(
            'Dtomasi\Tests\Containers\TestObjects\ParentClass',
            $c->findByClass('Dtomasi\Tests\Containers\TestObjects\ParentClass')[0]
        );
    }

    public function testSetAndFindByParentClass()
    {

        $c = new ObjectContainer();

        $c->addObject(new ParentClass());
        $c->addObject(new ChildClass());
        $c->addObject(new ChildClass());
        $c->addObject(new ChildChildClass());

        $this->assertTrue(
            is_array($c->findByParent('Dtomasi\Tests\Containers\TestObjects\ParentClass'))
            && count($c->findByParent('Dtomasi\Tests\Containers\TestObjects\ParentClass')) == 2);

        $this->assertInstanceOf(
            'Dtomasi\Tests\Containers\TestObjects\ChildClass',
            $c->findByParent('Dtomasi\Tests\Containers\TestObjects\ParentClass')[0]
        );

        $this->assertInstanceOf(
            'Dtomasi\Tests\Containers\TestObjects\ChildChildClass',
            $c->findByParent('Dtomasi\Tests\Containers\TestObjects\ChildClass')[0]
        );
    }
}
 