<?php
/**
 * 
 * @project: base-library
 * 
 * @author: Dtomasi
 * @date: 20.08.14
 * @time: 15:28
 *
 * @copyright: Fünfstern GmbH - Full-Service-Werbeagentur - Basel - Switzerland
 */

namespace Dtomasi\Tests\Containers\TestObjects;


use Dtomasi\Containers\ServiceInterface;

class TestService implements ServiceInterface {

    public function getName() {
        return 'test_service';
    }

    public function getTags() {
        return array('test','service','testservice');
    }
} 