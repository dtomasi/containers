<?php
/**
 * 
 * @project: base-library
 * 
 * @author: Dtomasi
 * @date: 20.08.14
 * @time: 15:27
 *
 * @copyright: Fünfstern GmbH - Full-Service-Werbeagentur - Basel - Switzerland
 */

namespace Dtomasi\Tests\Containers;


use Dtomasi\Containers\ServiceContainer;
use Dtomasi\Tests\Containers\TestObjects\TestService;
use Dtomasi\Containers\ServiceInterface;

class ServiceContainerTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var ServiceContainer
     */
    public $container;

    public function setUp() {
        $this->container = new ServiceContainer();
    }

    public function testAddAndGet() {

        $this->container->add(new TestService());
        $test = $this->container->get('test_service');

        $this->assertInstanceOf('Dtomasi\Containers\ServiceInterface', $test);
    }

    public function findByTags() {

    }

}
 